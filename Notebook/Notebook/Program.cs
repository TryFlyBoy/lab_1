﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class Program
    {
        static string InputNec(string input) // метод для реализации ввода обязательных параметров
        {
            Console.Write(input);
            bool check = true;
            string str = null;
            while (check)
            {
                str = Console.ReadLine();
                if (str != "" && !str.StartsWith(" "))
                {
                    check = false;
                }
                else
                {
                    Console.WriteLine("Это обязательное поле! Пожалуйста, заполните его.");
                }
            }
            return str;
        }

        static string InputUnNec(string input) // метод для реализации ввода необязательных параметров
        {
            Console.WriteLine(input);
            string str = Console.ReadLine();
            if (str == "" || str.StartsWith(" "))
            {
                return "—";
            }
            else
            {
                return str;
            }
        }

        static void Main(string[] args)
        {
            var records = new List<Record>();
            while (true)
            {
                bool check_end = false;
                Console.WriteLine("Вы начали работу с книгой записи");
                Console.WriteLine("Введите: \n  \"1\", чтобы создать запись");
                Console.WriteLine("  \"2\", чтобы отредактировать запись");
                Console.WriteLine("  \"3\", чтобы удалить запись");
                Console.WriteLine("  \"4\", чтобы просмотреть все записи");
                Console.WriteLine("  \"5\", чтобы просмотреть краткую информацию о всех записях");
                Console.WriteLine("  \"end\", чтобы закончить работу");
                Console.WriteLine("  Примечание для тестировщика: Введите \"test\", чтобы добавить несколько сформированных записей");
                string str = Console.ReadLine();
                switch (str)
                {
                    case "1":
                        records.Add(new Record(InputNec("Введите имя (обязательное поле!): "),
                        InputNec("Введите фамилию (обязательное поле!): "),
                        InputNec("Введите номер телефона (обязательное поле!): "),
                        InputNec("Введите страну (обязательное поле!): "),
                        InputUnNec("Введите отчество: "),
                        InputUnNec("Введите дату рождения: "),
                        InputUnNec("Введите организацию: "),
                        InputUnNec("Введите должность: "),
                        InputUnNec("Введите прочие заметки: ")));
                        Console.WriteLine("Запись успешно добавлена!");
                        break;

                    case "2":
                        Console.Write("Введите номер записи, которую хотите отредактировать: ");
                        int input = 0;
                        try
                        {
                            input = int.Parse(Console.ReadLine());

                            if (records.Exists(x => (x.ID == input)))
                            {
                                Record record = records[records.FindIndex(x => (x.ID == input))];
                                Console.WriteLine("Что вы хотите изменить?\n  Имя (введите \"1\"),\n  " +
                                    "Фамилию (введите \"2\"),\n  Отчество (введите \"3\"),\n  Номер телефона (введите \"4\"),\n  " +
                                    "Страну (введите \"5\"),\n  Дату рождения (введите \"6\"),\n  Организацию (введите \"7\"),\n  " +
                                    "Должность (введите \"8\"),\n  Прочие заметки (введите \"9\")\n  Введите \"back\", если передумали");
                                str = Console.ReadLine();
                                switch (str)
                                {
                                    case "1":
                                        record.Name = InputNec("Введите новое значение: ");
                                        break;
                                    case "2":
                                        record.Surname = InputNec("Введите новое значение: ");
                                        break;
                                    case "3":
                                        record.SecondName = InputUnNec("Введите новое значение: ");
                                        break;
                                    case "4":
                                        record.Phone = InputNec("Введите новое значение: ");
                                        break;
                                    case "5":
                                        record.Country = InputNec("Введите новое значение: ");
                                        break;
                                    case "6":
                                        record.Birthday = InputUnNec("Введите новое значение: ");
                                        break;
                                    case "7":
                                        record.Organisation = InputUnNec("Введите новое значение: ");
                                        break;
                                    case "8":
                                        record.Position = InputUnNec("Введите новое значение: ");
                                        break;
                                    case "9":
                                        record.Notes = InputUnNec("Введите новое значение: ");
                                        break;
                                    case "back":
                                        break;
                                    default:
                                        Console.WriteLine("Неопознанная команда! Попробуйте снова и введите одну из предложенных команд");
                                        break;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Записи с таким номером не существует!");
                            }
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Вы ввели некорректное значение!");
                        }
                        break;

                    case "3":
                        Console.Write("Введите номер записи, которую хотите удалить\n(Введите \"all\", " +
                            "чтобы удалить все записи, либо \"back\", если передумали): ");
                        str = Console.ReadLine();
                        if (str == "all")
                        {
                            records.Clear();
                            Console.WriteLine("Записи успешно удалены!");
                        }
                        else if (str == "back")
                        {
                            break;
                        }
                        else if (records.Exists(x => (x.ID == int.Parse(str))))
                        {
                            records.RemoveAll(x => (x.ID == int.Parse(str)));
                            Console.WriteLine("Запись успешно удалена!");
                        }
                        else
                        {
                            Console.WriteLine("Записи с таким номером не существует!");
                        }
                        break;

                    case "4":
                        if (records.Count == 0)
                        {
                            Console.WriteLine("В книге нет ни одной записи!");
                        }
                        foreach (Record x in records)
                        {
                            Console.WriteLine(x);
                        }
                        break;

                    case "5":
                        if (records.Count == 0)
                        {
                            Console.WriteLine("В книге нет ни одной записи!");
                        }
                        foreach (Record x in records)
                        {
                            x.ShowShortInfo();
                        }
                        break;
                    case "end":
                        check_end = true;
                        break;

                    // Эта команда предназначена для тестировщика
                    case "test":
                        records.Add(new Record("Artem", "Kozlov", "81234567665", "Russia"));
                        records.Add(new Record("John", "Anderson", "89453658725", "USA"));
                        records.Add(new Record("Jessie", "Iden", "85249743590", "England"));
                        records.Add(new Record("Li", "He", "85248539157", "China"));
                        Console.WriteLine("Записи успешно добавлены!");
                        break;
                    default:
                        Console.WriteLine("Неопознанная команда! Введите одну из предложенных команд");
                        break;
                }

                if(check_end)
                {
                    break;
                }

                Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
