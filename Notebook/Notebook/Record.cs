﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class Record
    {
        static int lastID = 0;
        public int ID;
        public string Name;
        public string Surname;
        public string SecondName;
        public string Phone;
        public string Country;
        public string Birthday;
        public string Organisation;
        public string Position;
        public string Notes;

        public Record(string name, string surname, string phone, string country, string secondName = "—", 
            string birthday = "—", string organization = "—", string position = "—", string notes = "—")
        {
            lastID++;
            ID = lastID;
            Name = name;
            Surname = surname;
            Phone = phone;
            Country = country;
            SecondName = secondName;
            Birthday = birthday;
            Organisation = organization;
            Position = position;
            Notes = notes;
        }
        public override string ToString()
        {
            Console.WriteLine();
            return "Запись № " + ID + "\nИмя: " + Name + "\nФамилия: " + Surname + "\nОтчество: " + 
                SecondName + "\nНомер телефона: " + Phone + "\nСтрана: " + Country + "\nДата рождения: " + 
                Birthday + "\nОрганизация: " + Organisation + "\nДолжность: " + Position + "\nПрочие заметки: " + Notes;
        }
        public void ShowShortInfo() // выводит краткую информацию о записях
        {
            Console.WriteLine();
            Console.WriteLine($"Запись № {ID}");
            Console.WriteLine($"Имя: {Name}");
            Console.WriteLine($"Фамилия: {Surname}");
            Console.WriteLine($"Номер телефона: {Phone}");
        }
    }
}
